## Description

This project provides an eventbus library.

An event has an id (which is basically a topic) and a scope. The scope is hierarchical. That means, an event published to "/abc" is visible for
handlers in "/abc/foo" also.

Furthermore, event handlers can be associated with notifier threads, such that an event gets always triggered from a specific thread. This
is for example useful if an event handler in a vaadin ui scope can register for any event but its event handle method is always triggered by the
ui local thread.

## Overview

This project consists of three different modules
* core
* spring
* ui

The core module provides basic functionallity and can be used in any java library project.

The spring module provides spring based integration. E.g. it provides an annotation which allows to automatically integrate beans as event handlers.

The ui module provides a basic integration into the vaadin ui framework. Mainly it ensures that event handlers registered at their
IUIEventBus are triggered by the UI local thread.
