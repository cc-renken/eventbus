/**
 * 
 */
package cc.renken.eventbus.spring.internal;

import org.springframework.stereotype.Service;

import cc.renken.eventbus.spring.IApplicationEventService;

/**
 * @author renkenh
 *
 */
@Service
public class ApplicationEventService extends ASpringEventService implements IApplicationEventService
{
	
}
