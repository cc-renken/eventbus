package cc.renken.eventbus.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="cc.renken.eventbus")
public class EventbusSpringBootAutoConfiguration
{
}
