/**
 * 
 */
package cc.renken.eventbus.spring.internal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cc.renken.eventbus.core.IEventHandler;

/**
 * NOT for public use - some bad workarounds for set based storage in ScopeNodes
 * (The object is the one with which equals and hashcode is executed, through this we prevent several MethodEventHandler for the same
 * object in one ScopeNode).
 * @author renkenh
 */
public class MethodEventHandler implements IEventHandler
{
	
	private final Method method;
	private final Object toCall;

	
	/**
	 * 
	 */
	public MethodEventHandler(Method method, Object toCall)
	{
		this.method = method;
		this.toCall = toCall;
	}


	@Override
	public void handleEvent(String eventID, String scope, Object... payload)
	{
		try
		{
			this.method.invoke(this.toCall, eventID, scope, payload);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof MethodEventHandler))
			return false;
		return this.toCall.equals(((MethodEventHandler) o).toCall);
	}

	@Override
	public int hashCode()
	{
		return this.toCall.hashCode();
	}
}
