/**
 * 
 */
package cc.renken.eventbus.spring.internal;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.renken.eventbus.core.IEventService;
import cc.renken.eventbus.core.internal.AEventNotifier;
import cc.renken.eventbus.core.internal.IInternalEventSystem;

/**
 * @author renkenh
 *
 */
@Component
public abstract class ASpringEventService extends AEventNotifier implements IEventService
{
	
	@Autowired
	private IInternalEventSystem eventSystem;

	@PostConstruct
	protected void postConstruct()
	{
		super.init();
	}

	@Override
	public void publishEvent(String eventID, Object... payload)
	{
		super.publishEvent(eventID, payload);
	}

	@Override
	public void publishEvent(String eventID, String scope, Object... payload)
	{
		super.publishEvent(eventID, scope, payload);
	}

	@PreDestroy
	protected void preDestroy()
	{
		super.dispose();
	}
	
	@Override
	protected IInternalEventSystem eventSystem()
	{
		return this.eventSystem;
	}

}
