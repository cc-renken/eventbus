package cc.renken.eventbus.spring;

import cc.renken.eventbus.core.EventConstants;
import cc.renken.eventbus.core.IEventService;


/**
 * Interface that should be used to autowire the application event service for application events.
 * @author renkenh
 */
public interface IApplicationEventService extends IEventService
{

	@Override
	public default String scope()
	{
		return EventConstants.ROOT_SCOPE;
	}
}