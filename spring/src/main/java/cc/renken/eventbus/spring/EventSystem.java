/**
 * 
 */
package cc.renken.eventbus.spring;

import cc.renken.eventbus.core.EventConstants;
import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.IEventService;
import cc.renken.eventbus.spring.internal.SpringInternalEventSystem;

/**
 * Simple static methods based event system for pojo objects. Uses {@link EventConstants#ROOT_SCOPE} as the notifier to register to.
 * @author renkenh
 */
public final class EventSystem
{

	/**
	 * Publish a given event to the given scope. Same as {@link IEventService#publishEvent(String, String, Object...)}, but a static call.
	 * @param eventID The event id.
	 * @param scope The scope to publish to.
	 * @param payload The payload. 
	 */
	public static final void publishEvent(String eventID, String scope, Object... payload)
	{
		SpringInternalEventSystem.instance().publishEvent(eventID, scope, payload);
	}

	
	/**
	 * Registers a handler to the given scope. Suitable events will be processed (triggered) by the root notifier.
	 * @param scope The scope to register for.
	 * @param handler The handler to register.
	 */
	public static final void registerEventHandler(String scope, IEventHandler handler)
	{
		SpringInternalEventSystem.instance().registerEventHandler(scope, EventConstants.ROOT_SCOPE, handler);
	}

	/**
	 * Unregister the given handler from the given scope.
	 * @param scope The scope to unregister from.
	 * @param handler The handler to unregister.
	 */
	public static final void unregisterEventHandler(String scope, IEventHandler handler)
	{
		SpringInternalEventSystem.instance().unregisterEventHandler(scope, EventConstants.ROOT_SCOPE, handler);
	}
	

	private EventSystem()
	{
		//no instance
	}

}
