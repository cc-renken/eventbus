/**
 * 
 */
package cc.renken.eventbus.spring.internal;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import cc.renken.eventbus.core.EventFilter;
import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.internal.InternalEventSystem;
import cc.renken.eventbus.spring.AtEventHandler;

/**
 * @author renkenh
 *
 */
@Service
public class SpringInternalEventSystem extends InternalEventSystem
{
	
	private static SpringInternalEventSystem INSTANCE;
	private static Class<?>[] METHOD_SIGNATURE = { String.class, String.class, Object[].class };
	
	public static final SpringInternalEventSystem instance()
	{
		return INSTANCE;
	}


	@Autowired
	private ApplicationContext context;
	
	private final Map<Class<?>, AtEventHandler> definitionCache = new HashMap<>();
	private final Map<Class<?>, Method> methodCache = new HashMap<>();

	
	@PostConstruct
	private void postConstruct()
	{
		INSTANCE = this;
	}

	@EventListener
    public void onApplicationEvent(ContextRefreshedEvent event)
	{
		Map<String, Object> beans = this.context.getBeansWithAnnotation(AtEventHandler.class);
		for (Object bean : beans.values())
		{
			AtEventHandler def = this.getDefinition(bean);
			IEventHandler handler = this.getHandler(bean);
			if (def.topics().length > 0)
				handler = new EventFilter(handler, def.topics()[0], def.topics());
			this.registerEventHandler(def.value(), def.notifierScope(), handler);
		}
    }
	
	private AtEventHandler getDefinition(Object bean)
	{
		AtEventHandler def = this.definitionCache.get(bean.getClass());
		if (def == null)
		{
			def = bean.getClass().getAnnotation(AtEventHandler.class);
			this.definitionCache.put(bean.getClass(), def);
		}
		return def;
	}
	
	private IEventHandler getHandler(Object bean)
	{
		if (bean instanceof IEventHandler)
			return (IEventHandler) bean;
		
		Method m = this.methodCache.get(bean.getClass());
		if (m == null)
		{
			for (Method toCheck : bean.getClass().getMethods())
			{
				if (Arrays.equals(toCheck.getParameterTypes(), METHOD_SIGNATURE))
				{
					m = toCheck;
					break;
				}
			}
			if (m == null) //no method found
				return null;
			this.methodCache.put(bean.getClass(), m);
		}
		return new MethodEventHandler(m, bean);
	}

	@PreDestroy
	private void preDestroy()
	{
		this.destroy();
	}

}
