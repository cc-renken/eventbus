/**
 * 
 */
package cc.renken.eventbus.spring;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

import cc.renken.eventbus.core.EventConstants;

@Documented
@Retention(RUNTIME)
@Target(TYPE)
/**
 * Annotation to mark a type as a Spring component that wants be automatically registered at the event system at creation time.
 * However, this works for application scope only!
 * 
 * The annotation processing system supports two different implementations:
 * First: Just derive from {@link IEventHandler} and you are good to go.
 * Second: Just have a public visible method that takes the following arguments: {@link String}, {@link String}, {@link Object}[]
 * 
 * @author renkenh
 */
@Component
public @interface AtEventHandler
{

	/**
	 * The scope of the event handler.
	 * @return The scope.
	 */
	public String value();

	/**
	 * Topics that the handler wants to receive. Everything else will be dropped. Leave blank for everything.
	 * @return The topics the handler is interested in.
	 */
	public String[] topics() default {};

	public String notifierScope() default EventConstants.ROOT_SCOPE;

}
