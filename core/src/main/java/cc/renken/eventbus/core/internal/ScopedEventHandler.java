/**
 * 
 */
package cc.renken.eventbus.core.internal;

import cc.renken.eventbus.core.IEventHandler;

/**
 * @author renkenh
 *
 */
class ScopedEventHandler implements IEventHandler
{
	
	private final String notifierScope;
	private final IEventHandler toWrap;

	/**
	 * 
	 */
	public ScopedEventHandler(String notifierScope, IEventHandler handler)
	{
		this.notifierScope = notifierScope;
		this.toWrap = handler;
	}

	
	public String notifierScope()
	{
		return this.notifierScope;
	}
	
	@Override
	public void handleEvent(String eventID, String scope, Object... payload)
	{
		this.toWrap.handleEvent(eventID, scope, payload);
	}

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof ScopedEventHandler))
			return false;
		
		return this.toWrap.equals(((ScopedEventHandler) o).toWrap);
	}

	@Override
	public int hashCode()
	{
		return this.toWrap.hashCode();
	}

	public String toString()
	{
		return this.toWrap.toString();
	}
}
