/**
 * 
 */
package cc.renken.eventbus.core.internal;

import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.IEventNotifier;

/**
 * Common interface for the internal event system. Should be used whenever possible.
 * This type specifies the main system for publishing events. Every {@link IEventNotifier} has to
 * use this interface to publish events. Only then it is guaranteed that the event reaches the necessary
 * {@link IEventHandler}s.
 * 
 * Every {@link IEventNotifier} should delegate its method to these. The event system will then take the necessary steps
 * to handle everything. 
 * @author renkenh
 */
public interface IInternalEventSystem
{

	/**
	 * Publish the event on the given scope.
	 * @param eventID The event topic/id.
	 * @param scope The scope on which to publish. If the scope is unknown, the event will be discarded.
	 * @param parameters The payload of the event.
	 */
	public void publishEvent(String eventID, String scope, Object... payload);


	/**
	 * Register an event notifier.
	 * @param notifier The notifier to register.
	 */
	public void registerEventNotifier(IInternalEventNotifier notifier);

	/**
	 * Unregister an event notifier.
	 * @param notifier The notifier to unregister.
	 */
	public void unregisterEventNotifier(IInternalEventNotifier notifier);


	/**
	 * Method to register a given handler on an arbitrary scope. Used by notifiers to allow the registering of handles on other scopes then their
	 * own. Prevents wild instance propagation (e.g. of the root scope into some subsystem).
	 * @param scope The scope for which to register the event handler.
	 * @param handler The handler to register.
	 */
	public void registerEventHandler(String scope, String notifierScope, IEventHandler handler);

	public void unregisterEventHandler(String scope, String notifierScope, IEventHandler handler);

}
