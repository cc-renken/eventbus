/**
 * 
 */
package cc.renken.eventbus.core.internal;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * ThreadFactory implementation that can be used with {@link ExecutorService}s.
 * Other than the default factory used in {@link Executors} this factory can create deamonized threads.
 * @author renkenh
 */
class ThreadFactoryImpl implements ThreadFactory
{

	private final AtomicInteger threadNumber = new AtomicInteger(0);
	
	private final ThreadGroup threadGroup;
	private final boolean daemon;


	/**
	 * Constructor.
	 * @param groupName The name of the {@link ThreadGroup} in which the threads should be created. May be <code>null</code> for no group.
	 * @param daemon Specifies whether this factory should create daemonized threads or not.
	 */
	public ThreadFactoryImpl(String groupName, boolean daemon)
	{
		this.threadGroup = new ThreadGroup(groupName);
		this.daemon = daemon;
	}


	@Override
	public Thread newThread(Runnable r)
	{
		Thread t = new Thread(this.threadGroup, r, this.threadGroup.getName() + this.threadNumber.getAndIncrement(), 0);
		t.setDaemon(this.daemon);
		return t;
	}

}
