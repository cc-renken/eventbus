/**
 * 
 */
package cc.renken.eventbus.core;

/**
 * Constants for the event framework.
 * @author renkenh
 */
public final class EventConstants
{
	
	/** The separator used for the scope ids */
	public static final String SCOPE_SEPARATOR = "/";
	
	/** Wildcard character */
	public static final String WILDCARD = "*";
	
	/** Scope id of the root scope. */
	public static final String ROOT_SCOPE = "";


}
