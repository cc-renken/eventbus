/**
 * 
 */
package cc.renken.eventbus.core;

import java.util.Arrays;

/**
 * Simple utils class to handle scope creation in a standardized way.
 * Scopes are arbitrary strings where the hierarchy is specified through {@link EventConstants#SCOPE_SEPARATOR}.
 * @author renkenh
 */
public class Scopes
{

	/**
	 * Create a scope that concatenates all given segments but separating them by {@link EventConstants#SCOPE_SEPARATOR}.
	 * @param first The first segment - if <code>null</code> null is returned. 
	 * @param more All other segments to concatenate to the first one. May be null or empty.
	 * @return A unified and normalized scope.
	 */
	public static final String get(String first, String... more)
	{
        if (more == null || more.length == 0 || first == null)
            return first;
        
        StringBuilder sb = new StringBuilder();
        sb.append(first);
        for (String path : more) {
            if (path.length() > 0) {
                if (sb.length() > 0) {
                    sb.append(EventConstants.SCOPE_SEPARATOR);
                }
                sb.append(path);
            }
        }
        String scope = sb.toString();
        scope = scope.replaceAll("["+escapedSeparator()+"]{2,}", EventConstants.SCOPE_SEPARATOR);
        return scope;
	}
	
	private static final String escapedSeparator()
	{
		if (EventConstants.SCOPE_SEPARATOR.equals("/"))
			return "\\/";
		return EventConstants.SCOPE_SEPARATOR;
	}
	
	/**
	 * Returns whether the given scope is a wildcard scope. That means, the scope equals {@link EventConstants#WILDCARD} or
	 * ends with {@link EventConstants#SCOPE_SEPARATOR} + {@link EventConstants#WILDCARD}.
	 * @param scope The scope to check.
	 * @return Whether the given scope is a wildcard scope or not.
	 */
	public static boolean isWildcard(String scope)
	{
		if (scope == null)
			return false;
		if (EventConstants.WILDCARD.equals(scope))
			return true;
		return scope.endsWith(EventConstants.SCOPE_SEPARATOR + EventConstants.WILDCARD);
	}
	
	/**
	 * Allows the extraction of all single segments of a scope. Hereby a wildcard at the end of the scope is ignored.
	 * Actual segments, i.e. parts separated by the {@link EventConstants#SCOPE_SEPARATOR} scope separator, are returned.
	 * That means, for the root path or a path consisting of a separator only, an array of size zero is returned.
	 * Several event scopes behind each other are ignored.
	 * 
	 * @param scope The scope to separate.
	 * @return An array containing all segments. If <code>null</code> is provided, <code>null</code> is returned. Leading and trailing whitespaces are ignored.
	 */
	public static String[] extractSegments(String scope)
	{
		if (scope == null)
			return null;
		scope = scope.trim();
		//strip leading and trailing scope separator
		if (scope.startsWith(EventConstants.SCOPE_SEPARATOR))
			scope = scope.substring(1);
		if (scope.endsWith(EventConstants.WILDCARD))
			scope = scope.substring(0, scope.length()-1);
		if (scope.endsWith(EventConstants.SCOPE_SEPARATOR))
			scope = scope.substring(0, scope.length()-1);
		String[] split = scope.split(EventConstants.SCOPE_SEPARATOR);
		return Arrays.stream(split).filter((s) -> s.length() > 0).toArray(String[]::new);
	}
	
	
	/**
	 * 
	 */
	private Scopes()
	{
		//no instance
	}

}
