/**
 * 
 */
package cc.renken.eventbus.core;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Simple event handler wrapper that filters for specific event ids or event topics. That means, it checks if a given event matches
 * a list of specified topics. If it matches, it calls the wrapped event handler, otherwise the event is dropped.
 * Because of logic used in the internal event framework, {@link #equals(Object)} and {@link #hashCode()} proxy for the
 * wrapped handler.
 * @author renkenh
 */
public final class EventFilter implements IEventHandler
{

	private final Set<String> topics = new HashSet<>();
	private final IEventHandler toWrap;


	/**
	 * Constructor.
	 */
	public EventFilter(IEventHandler toWrap, String topic, String... more)
	{
		this.topics.add(topic);
		this.topics.addAll(Arrays.asList(more));
		this.toWrap = toWrap;
	}

	@Override
	public void handleEvent(String eventID, String scope, Object... payload)
	{
		if (this.topics.contains(eventID))
			this.toWrap.handleEvent(eventID, scope, payload);
	}

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof EventFilter))
			return false;
		
		return this.toWrap.equals(((EventFilter) o).toWrap);
	}

	@Override
	public int hashCode()
	{
		return this.toWrap.hashCode();
	}

	public String toString()
	{
		return this.toWrap.toString();
	}
}
