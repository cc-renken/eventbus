/**
 * 
 */
package cc.renken.eventbus.core.internal;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.eventbus.core.EventConstants;
import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.Scopes;

/**
 * @author renkenh
 */
public class InternalEventSystem implements IInternalEventSystem
{

	protected static final Logger logger = LoggerFactory.getLogger(InternalEventSystem.class);

	
	/** The threadgroup name of the event {@link ThreadGroup}. */
	public static final String EVENT_THREADGROUP = "event";
	/**
	 * The factory used for all executors in the event package.
	 * Creates daemonized threads in a thread group with the name {@link #EVENT_THREADGROUP}.
	 */
	private static final ThreadFactory EVENT_FACTORY = new ThreadFactoryImpl(EVENT_THREADGROUP, true);


	private final ExecutorService executor = Executors.newSingleThreadExecutor(EVENT_FACTORY);
	private final ScopeNode root = new ScopeNode(EventConstants.ROOT_SCOPE, null);


	private ScopeNode loadScopeNode(String scope)
	{
		String[] segments = Scopes.extractSegments(scope);
		ScopeNode current = this.root;
		for (String segment : segments)
		{
			ScopeNode child = current.getChild(segment);
			if (child == null)
				child = new ScopeNode(segment, current);
			current = child;
		}
		return current;
	}
	
	@Override
	public void publishEvent(String eventID, String scope, Object... payload)
	{
		this.executor.execute(() ->
		{
			ScopeNode node = this.root.findScopeNode(scope);
			if (node != null)
				node.notifyHandlers(eventID, scope, payload);
		});
	}
	
	@Override
	public void registerEventHandler(String scope, String notifierScope, IEventHandler handler)
	{
		this.executor.execute(() ->
		{
			ScopeNode node = this.loadScopeNode(scope);
			node.addHandler(new ScopedEventHandler(notifierScope, handler), Scopes.isWildcard(scope));
		});
	}

	@Override
	public void unregisterEventHandler(String scope, String notifierScope, IEventHandler handler)
	{
		this.executor.execute(() ->
		{
			ScopeNode node = this.loadScopeNode(scope);
			node.removeHandler(handler);
		});
	}

	@Override
	public void registerEventNotifier(IInternalEventNotifier notifier)
	{
		this.executor.execute(() ->
		{
			ScopeNode node = this.loadScopeNode(notifier.scope());
			node.registerNotifier(notifier);
		});
	}

	@Override
	public void unregisterEventNotifier(IInternalEventNotifier notifier)
	{
		this.executor.execute(() ->
		{
			ScopeNode node = this.loadScopeNode(notifier.scope());
			node.dispose();
		});
	}

	/**
	 * Method to shutdown the whole event system. Afterwards this instance of event system cannot be used anymore.
	 */
	protected void destroy()
	{
		this.executor.execute(() ->
		{
			this.root.dispose();
			this.executor.shutdown();
		});
	}

	/**
	 * Enable a debug logging. This consumes resources as the payload of every event is transformed by reflection before the logging level is checked.
	 * Therefore, this method must be called to actually enable the debug logging.
	 * @return Always true such that this can be used with asserts, like: assert enableDebugging();
	 */
	protected boolean enableDebugging()
	{
		this.registerEventHandler(Scopes.get(EventConstants.ROOT_SCOPE, EventConstants.WILDCARD), EventConstants.ROOT_SCOPE, (eventID, scope, payload) ->
		{
			logger.debug("Triggered Event [{}:{}]: {}", scope, eventID, payload);
		});
		return true;
	}
}
