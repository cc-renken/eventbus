/**
 * 
 */
package cc.renken.eventbus.core;

/**
 * Implement this interface if the type should notify others about events.
 * @author renkenh
 */
public interface IEventNotifier
{

	/** The scope for which this notifier is responsible. */
	public String scope();
	

	/**
	 * Registers a new handler for events pushed by this connector.
	 * Calls {@link #registerEventHandler(String, IEventHandler)} with {@link #scope()} as the scope.
	 * @param handler The handler to register.
	 */
	public default void registerEventHandler(IEventHandler handler)
	{
		this.registerEventHandler(this.scope(), handler);
	}

	/**
	 * Unregisters a handler from events pushed by this connector. The method may be called several times, i.e.
	 * without the handler being registered.
	 * Calls {@link #unregisterEventHandler(String, IEventHandler)} with {@link #scope()} as the scope.
	 * @param handler The handler to unregister.
	 */
	public default void unregisterEventHandler(IEventHandler handler)
	{
		this.unregisterEventHandler(this.scope(), handler);
	}


	/**
	 * Registers the given event handler for the given scope. The event handler will be called by this event notifier for
	 * the given scope. That means, with calling this method, this notifier is responsible for this handler to be called for
	 * the given scope.
	 * Please note, that /abc/* and /abc are "different" scopes for which one can register.
	 * "/abc/*" also matches /abc/foo and /abc/bar and of course /abc.
	 * Whereas /abc matches /abc only.
	 * @param scope The scope for which to register. May have an {@link EventConstants#WILDCARD} at the end.
	 * @param handler The handler to register.
	 */
	public void registerEventHandler(String scope, IEventHandler handler);

	/**
	 * Unregisters the given event handler for the given scope.
	 * @param scope The scope for which to unregister the event handler.
	 * @param handler The handler to unregister.
	 */
	public void unregisterEventHandler(String scope, IEventHandler handler);

}
