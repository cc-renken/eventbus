package cc.renken.eventbus.core;


/**
 * Service interface that allows arbitrary logic to publish an event on a specific scope.
 * @author renkenh
 */
public interface IEventService extends IEventNotifier
{

	/**
	 * Publishes an event on the scope specified by this notifier ({@link #scope()}).
	 * Same as calling {@link #publishEvent(String, String, Object...)} with scope equals to {@link #scope()}.
	 * @param eventID The event id (or topic)
	 * @param payload The payload to send.
	 */
	public default void publishEvent(String eventID, Object... payload)
	{
		this.publishEvent(eventID, this.scope(), payload);
	}

	/**
	 * Allows publishing an event to an arbitrary scope. All event handlers registered for this scope will be notified on the event. Furthermore,
	 * they will be triggered by their responsible event notifier (if any).
	 * @param eventID The event id (or topic)
	 * @param scope The scope of the event.
	 * @param payload The payload to send.
	 */
	public void publishEvent(String eventID, String scope, Object... payload);

}