/**
 * 
 */
package cc.renken.eventbus.core.internal;

import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.IEventNotifier;

/**
 * @author renkenh
 *
 */
public interface IInternalEventNotifier extends IEventNotifier
{
	
	/**
	 * Method, used to inform the handlers about an event. This method is called by the event system. Implement this method 
	 * in such a way that for this scope fits. E.g. if the subsystem represented by this notifier uses its own thread and is ThreadLocal
	 * this specific thread can be used to publish the event. This helps to keep the code in the handlers small as they do not have to worry
	 * about threading.
	 * @param eventID The event topic/id.
	 * @param scope The scope of the event.
	 * @param payload The payload of the event.
	 */
	public void notifyHandler(IEventHandler handler, String eventID, String scope, Object... payload);

	
}
