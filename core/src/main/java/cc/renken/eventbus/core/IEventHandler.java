package cc.renken.eventbus.core;

/**
 * Type that describes an API to receive events send out by a connector.
 * @author renkenh
 */
@FunctionalInterface
public interface IEventHandler
{

	/**
	 * Called by the thread that originally called the connector to push an event to the handler.
	 * The handler must ensure the thread safety of his implementation!
	 * @param eventID The event ID. See {@link IConnector} for IDs and their description.
	 * @param scope The scope of the event. This is need because the same handler can be registered for different scopes.
	 * @param payload The parameters belonging to the given event. The parameters are immutable.
	 */
	public void handleEvent(String eventID, String scope, Object... payload);

}