/**
 * 
 */
package cc.renken.eventbus.core.internal;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import cc.renken.eventbus.core.EventConstants;
import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.Scopes;

/**
 * NOT thread-save
 * @author renkenh
 */
public class ScopeNode
{
	
	private static final AEventNotifier NO_NOTIFIER = new AEventNotifier()
	{
		
		@Override
		public String scope()
		{
			return null;
		}
		
		@Override
		protected IInternalEventSystem eventSystem()
		{
			return null;
		}
	};


	private final Set<ScopedEventHandler> handlers = new LinkedHashSet<>();
	private final Set<ScopedEventHandler> wildHandlers = new LinkedHashSet<>();

	private IInternalEventNotifier notifier;
	private final String id;
	private ScopeNode parent;
	private final Map<String, ScopeNode> children = new HashMap<>(2);


	/**
	 * 
	 */
	public ScopeNode(String id, ScopeNode parent)
	{
		this.id = id;
		this.parent = parent;
		if (parent != null)
			this.parent.children.put(id, this);
	}
	
	
	public String scope()
	{
		return (this.parent != null ? this.parent.scope() + EventConstants.SCOPE_SEPARATOR : "") + this.id;
	}

	public ScopeNode getParent()
	{
		return this.parent;
	}

	public ScopeNode getChild(String childId)
	{
		return this.children.get(childId);
	}

	public ScopeNode findScopeNode(String scope)
	{
		String[] segments = Scopes.extractSegments(scope);
		ScopeNode current = this;
		for (String segment : segments)
		{
			ScopeNode child = current.getChild(segment);
			if (child == null)
				return null;
			current = child;
		}
		return current;
	}


	public void addHandler(ScopedEventHandler handler, boolean wildcard)
	{
		if (!wildcard)
			this.handlers.add(handler);
		else
			this.wildHandlers.add(handler);
	}

	private enum HandlerSet
	{
		NORMAL,
		WILDCARD,
		ALL
	}

	private final Collection<ScopedEventHandler> getHandlers(HandlerSet handlerSet)
	{
		LinkedHashSet<ScopedEventHandler> set = new LinkedHashSet<>();
		switch (handlerSet)
		{
			case ALL:
				set.addAll(this.wildHandlers);
			case NORMAL:
				set.addAll(this.handlers);
				break;
			case WILDCARD:
				set.addAll(this.wildHandlers);
				break;
		}
		return set;
	}
	
	public void removeHandler(IEventHandler handler)
	{
		this.handlers.remove(handler);
		this.wildHandlers.remove(handler);
	}

	public void registerNotifier(IInternalEventNotifier notifier)
	{
		if (this.notifier != null)
			throw new IllegalStateException("Scope '" + this.scope() + "' has a notifier already. Notifier to register: " + notifier.getClass().getName());
		this.notifier = notifier;
	}

	public void notifyHandlers(String eventID, String scope, Object... payload)
	{
		this.notifyHandlers(HandlerSet.ALL, eventID, scope, payload);
		this.notifyParentHandlers(eventID, scope, payload);
		this.notifyChildrenHandlers(eventID, scope, payload);
	}
	
	private void notifyParentHandlers(String eventID, String scope, Object... payload)
	{
		if (this.parent != null)
		{
			this.parent.notifyHandlers(HandlerSet.WILDCARD, eventID, scope, payload);
			this.parent.notifyParentHandlers(eventID, scope, payload);
		}
	}
	
	private void notifyChildrenHandlers(String eventID, String scope, Object... payload)
	{
		for (ScopeNode child : this.children.values())
		{
			child.notifyHandlers(HandlerSet.ALL, eventID, scope, payload);
			child.notifyChildrenHandlers(eventID, scope, payload);
		}
	}
	
	private final void notifyHandlers(HandlerSet set, String eventID, String scope, Object... payload)
	{
		Collection<ScopedEventHandler> handlers = this.getHandlers(set);
		for (ScopedEventHandler handler : handlers)
		{
			IInternalEventNotifier notifier = this.findResponsibleNotifier(handler.notifierScope());
			if (notifier != null)
				notifier.notifyHandler(handler, eventID, scope, payload);
		}
	}

	private IInternalEventNotifier findResponsibleNotifier(String scope)
	{
		ScopeNode node = this.getRoot().findScopeNode(scope);
		if (node == null)
			return null;
		return node.findResponsibleNotifier();
	}
	
	private ScopeNode getRoot()
	{
		if (this.parent != null)
			return this.parent.getRoot();
		return this;
	}

	private IInternalEventNotifier findResponsibleNotifier()
	{
		IInternalEventNotifier notifier = this.notifier;
		if (notifier != null)
			return notifier;
		if (this.parent != null)
			return this.parent.findResponsibleNotifier();
		return NO_NOTIFIER;
	}
	
	public void dispose()
	{
		this.handlers.clear();
		this.wildHandlers.clear();
		if (this.parent != null)
			this.parent.children.remove(this.id);
		this.parent = null;
		this.notifier = null;
		for (ScopeNode node : this.children.values().toArray(new ScopeNode[this.children.size()]))
			node.dispose();
		this.children.clear();
	}
	
}
