/**
 * 
 */
package cc.renken.eventbus.core.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.eventbus.core.IEventHandler;

/**
 * Abstract implementation that does all the listener management. However, does not provide any method to publish events.
 * Must be implemented by the derived class or {@link EventNotifierStub} must be used.
 * @author renkenh
 */
public abstract class AEventNotifier implements IInternalEventNotifier
{
	
	protected static final Logger logger = LoggerFactory.getLogger(AEventNotifier.class);
	

	protected abstract IInternalEventSystem eventSystem();


	protected final void init()
	{
		this.eventSystem().registerEventNotifier(this);
	}

	protected final void dispose()
	{
		this.eventSystem().unregisterEventNotifier(this);
	}


	protected void publishEvent(String eventID, Object... payload)
	{
		this.publishEvent(eventID, this.scope(), payload);
	}

	protected void publishEvent(String eventID, String scope, Object... payload)
	{
		this.eventSystem().publishEvent(eventID, scope, payload);
	}


	@Override
	public void notifyHandler(IEventHandler handler, String eventID, String scope, Object... payload)
	{
		try
		{
			handler.handleEvent(eventID, scope, payload);
		}
		catch (Exception ex)
		{
			logger.error("Handler {} threw an exception.", handler, ex);
		}
	}
	
	@Override
	public void registerEventHandler(String scope, IEventHandler handler)
	{
		this.eventSystem().registerEventHandler(scope, this.scope(), handler);
	}

	@Override
	public void unregisterEventHandler(String scope, IEventHandler handler)
	{
		this.eventSystem().unregisterEventHandler(scope, this.scope(), handler);
	}

}
