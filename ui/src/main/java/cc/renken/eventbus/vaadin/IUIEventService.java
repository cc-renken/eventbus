/**
 * 
 */
package cc.renken.eventbus.vaadin;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.stereotype.Service;

import com.vaadin.spring.annotation.UIScope;

import cc.renken.eventbus.core.IEventService;

/**
 * There will be one event service for every UI. Use this type for {@link Autowire}.
 * Mainly, this service will execute every handler notification within the local UI thread.
 * @author renkenh
 */
@UIScope
@Service
public interface IUIEventService extends IEventService
{

}
