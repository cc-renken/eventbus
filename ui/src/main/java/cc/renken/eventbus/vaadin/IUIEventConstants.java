package cc.renken.eventbus.vaadin;

import cc.renken.eventbus.core.EventConstants;
import cc.renken.eventbus.core.Scopes;

/**
 * Constants for the UI Eventbus Framework.
 * @author renkenh
 */
public interface IUIEventConstants
{

	/**
	 * The main scope for all UIs. Every UI will be located beneath this scope.
	 */
	public static final String MAIN_UI_SCOPE = Scopes.get(EventConstants.ROOT_SCOPE, "ui");


}
