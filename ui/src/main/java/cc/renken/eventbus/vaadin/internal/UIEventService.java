/**
 * 
 */
package cc.renken.eventbus.vaadin.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.UI;

import cc.renken.eventbus.core.IEventHandler;
import cc.renken.eventbus.core.Scopes;
import cc.renken.eventbus.spring.internal.ASpringEventService;
import cc.renken.eventbus.vaadin.IUIEventConstants;
import cc.renken.eventbus.vaadin.IUIEventService;

/**
 * Implementation of the {@link IUIEventService}. There will be one event service for every UI.
 * Mainly, this service will execute every handler notification within the local UI thread.
 * @author renkenh
 */
@UIScope
@Service
public final class UIEventService extends ASpringEventService implements IUIEventService
{

	@Autowired
	private UI ui;


	@Override
	public void notifyHandler(IEventHandler handler, String eventID, String scope, Object... payload)
	{
		this.ui.access(() -> handler.handleEvent(eventID, scope, payload));
	}
	
	@Override
	public String scope()
	{
		return Scopes.get(IUIEventConstants.MAIN_UI_SCOPE, this.ui.getSession().getSession().getId(), String.valueOf(this.ui.getUIId()));
	}

}
